<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * KidsTripsFixture
 */
class KidsTripsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'kids_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'trips_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'trips_id' => ['type' => 'index', 'columns' => ['trips_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['kids_id', 'trips_id'], 'length' => []],
            'kids_trips_ibfk_1' => ['type' => 'foreign', 'columns' => ['kids_id'], 'references' => ['kids', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'kids_trips_ibfk_2' => ['type' => 'foreign', 'columns' => ['trips_id'], 'references' => ['trips', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'kids_id' => 1,
                'trips_id' => 1
            ],
        ];
        parent::init();
    }
}
