<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'first_name' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'last_name' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'email' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'role' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'password' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'families_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'kids_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tutors_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'teachers_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_users_families1' => ['type' => 'index', 'columns' => ['families_id'], 'length' => []],
            'fk_users_kids1' => ['type' => 'index', 'columns' => ['kids_id'], 'length' => []],
            'fk_users_tutors1' => ['type' => 'index', 'columns' => ['tutors_id'], 'length' => []],
            'fk_users_teachers1' => ['type' => 'index', 'columns' => ['teachers_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_users_families1' => ['type' => 'foreign', 'columns' => ['families_id'], 'references' => ['families', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_users_kids1' => ['type' => 'foreign', 'columns' => ['kids_id'], 'references' => ['kids', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_users_teachers1' => ['type' => 'foreign', 'columns' => ['teachers_id'], 'references' => ['teachers', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_users_tutors1' => ['type' => 'foreign', 'columns' => ['tutors_id'], 'references' => ['tutors', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'first_name' => 'Lorem ipsum dolor sit amet',
                'last_name' => 'Lorem ipsum dolor sit amet',
                'email' => 'Lorem ipsum dolor sit amet',
                'role' => 1,
                'password' => 'Lorem ipsum dolor sit amet',
                'families_id' => 1,
                'kids_id' => 1,
                'tutors_id' => 1,
                'teachers_id' => 1
            ],
        ];
        parent::init();
    }
}
