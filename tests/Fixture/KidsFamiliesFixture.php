<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * KidsFamiliesFixture
 */
class KidsFamiliesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'kids_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'families_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_kids_has_Family_Family1' => ['type' => 'index', 'columns' => ['families_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['kids_id', 'families_id'], 'length' => []],
            'fk_kids_has_Family_Family1' => ['type' => 'foreign', 'columns' => ['families_id'], 'references' => ['families', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_kids_has_Family_kids1' => ['type' => 'foreign', 'columns' => ['kids_id'], 'references' => ['kids', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'kids_id' => 1,
                'families_id' => 1
            ],
        ];
        parent::init();
    }
}
