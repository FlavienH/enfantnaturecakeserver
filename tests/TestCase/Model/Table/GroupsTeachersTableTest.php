<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GroupsTeachersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GroupsTeachersTable Test Case
 */
class GroupsTeachersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\GroupsTeachersTable
     */
    public $GroupsTeachers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.GroupsTeachers',
        'app.Groups',
        'app.Teachers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('GroupsTeachers') ? [] : ['className' => GroupsTeachersTable::class];
        $this->GroupsTeachers = TableRegistry::getTableLocator()->get('GroupsTeachers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GroupsTeachers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
