<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PostsImagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PostsImagesTable Test Case
 */
class PostsImagesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PostsImagesTable
     */
    public $PostsImages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PostsImages',
        'app.Posts',
        'app.Images'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PostsImages') ? [] : ['className' => PostsImagesTable::class];
        $this->PostsImages = TableRegistry::getTableLocator()->get('PostsImages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PostsImages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
