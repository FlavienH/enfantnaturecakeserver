<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ActivitiesSchoolTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ActivitiesSchoolTable Test Case
 */
class ActivitiesSchoolTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ActivitiesSchoolTable
     */
    public $ActivitiesSchool;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ActivitiesSchool',
        'app.Activities',
        'app.Schools'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ActivitiesSchool') ? [] : ['className' => ActivitiesSchoolTable::class];
        $this->ActivitiesSchool = TableRegistry::getTableLocator()->get('ActivitiesSchool', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ActivitiesSchool);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
