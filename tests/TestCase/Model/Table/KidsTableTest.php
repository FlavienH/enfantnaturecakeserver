<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\KidsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\KidsTable Test Case
 */
class KidsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\KidsTable
     */
    public $Kids;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Kids',
        'app.Tutors',
        'app.Images',
        'app.Videos',
        'app.Posts',
        'app.Families',
        'app.Groups'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Kids') ? [] : ['className' => KidsTable::class];
        $this->Kids = TableRegistry::getTableLocator()->get('Kids', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Kids);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
