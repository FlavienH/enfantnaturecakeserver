<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\KidsTripsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\KidsTripsTable Test Case
 */
class KidsTripsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\KidsTripsTable
     */
    public $KidsTrips;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.KidsTrips',
        'app.Kids',
        'app.Trips'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('KidsTrips') ? [] : ['className' => KidsTripsTable::class];
        $this->KidsTrips = TableRegistry::getTableLocator()->get('KidsTrips', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->KidsTrips);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
