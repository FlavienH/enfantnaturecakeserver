<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\KidsFamiliesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\KidsFamiliesTable Test Case
 */
class KidsFamiliesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\KidsFamiliesTable
     */
    public $KidsFamilies;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.KidsFamilies',
        'app.Kids',
        'app.Families'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('KidsFamilies') ? [] : ['className' => KidsFamiliesTable::class];
        $this->KidsFamilies = TableRegistry::getTableLocator()->get('KidsFamilies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->KidsFamilies);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
