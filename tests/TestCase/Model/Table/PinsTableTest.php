<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PinsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PinsTable Test Case
 */
class PinsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PinsTable
     */
    public $Pins;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Pins'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Pins') ? [] : ['className' => PinsTable::class];
        $this->Pins = TableRegistry::getTableLocator()->get('Pins', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pins);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
