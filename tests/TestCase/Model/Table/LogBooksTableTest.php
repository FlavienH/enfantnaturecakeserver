<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LogBooksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LogBooksTable Test Case
 */
class LogBooksTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\LogBooksTable
     */
    public $LogBooks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.LogBooks',
        'app.Kids',
        'app.Teachers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('LogBooks') ? [] : ['className' => LogBooksTable::class];
        $this->LogBooks = TableRegistry::getTableLocator()->get('LogBooks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LogBooks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
