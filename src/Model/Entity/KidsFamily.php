<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * KidsFamily Entity
 *
 * @property int $kids_id
 * @property int $families_id
 *
 * @property \App\Model\Entity\Kid $kid
 * @property \App\Model\Entity\Family $family
 */
class KidsFamily extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'kid' => true,
        'family' => true
    ];
}
