<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Kid Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $birth_date
 * @property string $address
 * @property int|null $phone_number
 * @property string|null $photo
 * @property string|null $language
 * @property string|null $origin_country
 * @property \Cake\I18n\FrozenTime|null $in_canada_since
 * @property string|null $health_issues
 * @property int|null $insurance_number
 * @property int|null $tutors_id
 * @property int|null $images_id
 * @property int|null $videos_id
 * @property int|null $posts_id
 *
 * @property \App\Model\Entity\Tutor $tutor
 * @property \App\Model\Entity\Image $image
 * @property \App\Model\Entity\Video $video
 * @property \App\Model\Entity\Post $post
 * @property \App\Model\Entity\Family[] $families
 * @property \App\Model\Entity\Group[] $groups
 */
class Kid extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'birth_date' => true,
        'address' => true,
        'phone_number' => true,
        'photo' => true,
        'language' => true,
        'origin_country' => true,
        'in_canada_since' => true,
        'health_issues' => true,
        'insurance_number' => true,
        'tutors_id' => true,
        'images_id' => true,
        'videos_id' => true,
        'posts_id' => true,
        'tutor' => true,
        'image' => true,
        'video' => true,
        'post' => true,
        'families' => true,
        'groups' => true
    ];
}
