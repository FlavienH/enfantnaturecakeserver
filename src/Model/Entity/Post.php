<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Post Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $created
 * @property string|null $text
 * @property int $visibility
 * @property int|null $groups_id
 *
 * @property \App\Model\Entity\Group $group
 * @property \App\Model\Entity\Image[] $images
 * @property \App\Model\Entity\Video[] $videos
 */
class Post extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'text' => true,
        'visibility' => true,
        'groups_id' => true,
        'group' => true,
        'images' => true,
        'videos' => true
    ];
}
