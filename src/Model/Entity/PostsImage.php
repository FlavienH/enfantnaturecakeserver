<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PostsImage Entity
 *
 * @property int $post_id
 * @property int $image_id
 *
 * @property \App\Model\Entity\Post $post
 * @property \App\Model\Entity\Image $image
 */
class PostsImage extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'post' => true,
        'image' => true
    ];
}
