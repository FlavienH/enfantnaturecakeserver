<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Family Entity
 *
 * @property int $id
 * @property string|null $address
 * @property int|null $phone_number
 * @property string|null $photo
 * @property string|null $language
 * @property string|null $origin_country
 * @property \Cake\I18n\FrozenTime|null $in_canada_since
 *
 * @property \App\Model\Entity\Kid[] $kids
 */
class Family extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'address' => true,
        'phone_number' => true,
        'photo' => true,
        'language' => true,
        'origin_country' => true,
        'in_canada_since' => true,
        'kids' => true
    ];
}
