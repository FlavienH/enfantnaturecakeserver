<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $role
 * @property string $password
 * @property int|null $families_id
 * @property int|null $kids_id
 * @property int|null $tutors_id
 * @property int|null $teachers_id
 *
 * @property \App\Model\Entity\Family $family
 * @property \App\Model\Entity\Kid $kid
 * @property \App\Model\Entity\Tutor $tutor
 * @property \App\Model\Entity\Teacher $teacher
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'last_name' => true,
        'email' => true,
        'role' => true,
        'password' => true,
        'families_id' => true,
        'kids_id' => true,
        'tutors_id' => true,
        'teachers_id' => true,
        'family' => true,
        'kid' => true,
        'tutor' => true,
        'teacher' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
