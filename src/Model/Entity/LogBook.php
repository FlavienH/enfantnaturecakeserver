<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LogBook Entity
 *
 * @property int $id
 * @property string $content
 * @property string $name
 * @property int|null $kids_id
 * @property int|null $teachers_id
 * @property \Cake\I18n\FrozenDate $created
 * @property \Cake\I18n\FrozenDate|null $modified
 *
 * @property \App\Model\Entity\Kid $kid
 * @property \App\Model\Entity\Teacher $teacher
 */
class LogBook extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'content' => true,
        'name' => true,
        'kids_id' => true,
        'teachers_id' => true,
        'created' => true,
        'modified' => true,
        'kid' => true,
        'teacher' => true
    ];
}
