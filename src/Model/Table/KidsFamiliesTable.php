<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * KidsFamilies Model
 *
 * @property \App\Model\Table\KidsTable|\Cake\ORM\Association\BelongsTo $Kids
 * @property \App\Model\Table\FamiliesTable|\Cake\ORM\Association\BelongsTo $Families
 *
 * @method \App\Model\Entity\KidsFamily get($primaryKey, $options = [])
 * @method \App\Model\Entity\KidsFamily newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\KidsFamily[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\KidsFamily|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\KidsFamily saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\KidsFamily patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\KidsFamily[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\KidsFamily findOrCreate($search, callable $callback = null, $options = [])
 */
class KidsFamiliesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('kids_families');
        $this->setDisplayField('kids_id');
        $this->setPrimaryKey(['kids_id', 'families_id']);

        $this->belongsTo('Kids', [
            'foreignKey' => 'kids_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Families', [
            'foreignKey' => 'families_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['kids_id'], 'Kids'));
        $rules->add($rules->existsIn(['families_id'], 'Families'));

        return $rules;
    }
}
