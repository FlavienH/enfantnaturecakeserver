<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ActivitiesSchool Model
 *
 * @property \App\Model\Table\ActivitiesTable|\Cake\ORM\Association\BelongsTo $Activities
 * @property \App\Model\Table\SchoolsTable|\Cake\ORM\Association\BelongsTo $Schools
 *
 * @method \App\Model\Entity\ActivitiesSchool get($primaryKey, $options = [])
 * @method \App\Model\Entity\ActivitiesSchool newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ActivitiesSchool[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ActivitiesSchool|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActivitiesSchool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActivitiesSchool patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ActivitiesSchool[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ActivitiesSchool findOrCreate($search, callable $callback = null, $options = [])
 */
class ActivitiesSchoolTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('activities_school');
        $this->setDisplayField('activities_id');
        $this->setPrimaryKey(['activities_id', 'schools_id']);

        $this->belongsTo('Activities', [
            'foreignKey' => 'activities_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Schools', [
            'foreignKey' => 'schools_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['activities_id'], 'Activities'));
        $rules->add($rules->existsIn(['schools_id'], 'Schools'));

        return $rules;
    }
}
