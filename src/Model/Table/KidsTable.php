<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Kids Model
 *
 * @property \App\Model\Table\TutorsTable|\Cake\ORM\Association\BelongsTo $Tutors
 * @property \App\Model\Table\ImagesTable|\Cake\ORM\Association\BelongsTo $Images
 * @property \App\Model\Table\VideosTable|\Cake\ORM\Association\BelongsTo $Videos
 * @property \App\Model\Table\PostsTable|\Cake\ORM\Association\BelongsTo $Posts
 * @property \App\Model\Table\FamiliesTable|\Cake\ORM\Association\BelongsToMany $Families
 * @property \App\Model\Table\GroupsTable|\Cake\ORM\Association\BelongsToMany $Groups
 *
 * @method \App\Model\Entity\Kid get($primaryKey, $options = [])
 * @method \App\Model\Entity\Kid newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Kid[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Kid|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Kid saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Kid patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Kid[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Kid findOrCreate($search, callable $callback = null, $options = [])
 */
class KidsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('kids');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Tutors', [
            'foreignKey' => 'tutors_id'
        ]);
        $this->belongsTo('Images', [
            'foreignKey' => 'images_id'
        ]);
        $this->belongsTo('Videos', [
            'foreignKey' => 'videos_id'
        ]);
        $this->belongsTo('Posts', [
            'foreignKey' => 'posts_id'
        ]);
        $this->belongsToMany('Families', [
            'foreignKey' => 'kid_id',
            'targetForeignKey' => 'family_id',
            'joinTable' => 'kids_families'
        ]);
        $this->belongsToMany('Groups', [
            'foreignKey' => 'kid_id',
            'targetForeignKey' => 'group_id',
            'joinTable' => 'kids_groups'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->dateTime('birth_date')
            ->requirePresence('birth_date', 'create')
            ->allowEmptyDateTime('birth_date', false);

        $validator
            ->scalar('address')
            ->maxLength('address', 255)
            ->requirePresence('address', 'create')
            ->allowEmptyString('address', false);

        $validator
            ->integer('phone_number')
            ->allowEmptyString('phone_number');

        $validator
            ->scalar('photo')
            ->maxLength('photo', 16777215)
            ->allowEmptyString('photo');

        $validator
            ->scalar('language')
            ->maxLength('language', 255)
            ->allowEmptyString('language');

        $validator
            ->scalar('origin_country')
            ->maxLength('origin_country', 45)
            ->allowEmptyString('origin_country');

        $validator
            ->dateTime('in_canada_since')
            ->allowEmptyDateTime('in_canada_since');

        $validator
            ->scalar('health_issues')
            ->allowEmptyString('health_issues');

        $validator
            ->integer('insurance_number')
            ->allowEmptyString('insurance_number');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tutors_id'], 'Tutors'));
        $rules->add($rules->existsIn(['images_id'], 'Images'));
        $rules->add($rules->existsIn(['videos_id'], 'Videos'));
        $rules->add($rules->existsIn(['posts_id'], 'Posts'));

        return $rules;
    }
}
