<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GroupsTeachers Model
 *
 * @property \App\Model\Table\GroupsTable|\Cake\ORM\Association\BelongsTo $Groups
 * @property \App\Model\Table\TeachersTable|\Cake\ORM\Association\BelongsTo $Teachers
 *
 * @method \App\Model\Entity\GroupsTeacher get($primaryKey, $options = [])
 * @method \App\Model\Entity\GroupsTeacher newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\GroupsTeacher[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GroupsTeacher|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GroupsTeacher saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GroupsTeacher patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GroupsTeacher[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\GroupsTeacher findOrCreate($search, callable $callback = null, $options = [])
 */
class GroupsTeachersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('groups_teachers');
        $this->setDisplayField('groups_id');
        $this->setPrimaryKey(['groups_id', 'teachers_id']);

        $this->belongsTo('Groups', [
            'foreignKey' => 'groups_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Teachers', [
            'foreignKey' => 'teachers_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['groups_id'], 'Groups'));
        $rules->add($rules->existsIn(['teachers_id'], 'Teachers'));

        return $rules;
    }
}
