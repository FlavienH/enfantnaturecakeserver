<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * KidsGroups Model
 *
 * @property \App\Model\Table\KidsTable|\Cake\ORM\Association\BelongsTo $Kids
 * @property \App\Model\Table\GroupsTable|\Cake\ORM\Association\BelongsTo $Groups
 *
 * @method \App\Model\Entity\KidsGroup get($primaryKey, $options = [])
 * @method \App\Model\Entity\KidsGroup newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\KidsGroup[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\KidsGroup|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\KidsGroup saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\KidsGroup patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\KidsGroup[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\KidsGroup findOrCreate($search, callable $callback = null, $options = [])
 */
class KidsGroupsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('kids_groups');
        $this->setDisplayField('kids_id');
        $this->setPrimaryKey(['kids_id', 'groups_id']);

        $this->belongsTo('Kids', [
            'foreignKey' => 'kids_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Groups', [
            'foreignKey' => 'groups_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['kids_id'], 'Kids'));
        $rules->add($rules->existsIn(['groups_id'], 'Groups'));

        return $rules;
    }
}
