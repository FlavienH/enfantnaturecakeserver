<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pins Model
 *
 * @method \App\Model\Entity\Pin get($primaryKey, $options = [])
 * @method \App\Model\Entity\Pin newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Pin[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Pin|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pin saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Pin patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Pin[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Pin findOrCreate($search, callable $callback = null, $options = [])
 */
class PinsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pins');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('titre')
            ->maxLength('titre', 45)
            ->requirePresence('titre', 'create')
            ->allowEmptyString('titre', false);

        $validator
            ->scalar('type')
            ->maxLength('type', 45)
            ->requirePresence('type', 'create')
            ->allowEmptyString('type', false);

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        $validator
            ->numeric('longitude')
            ->requirePresence('longitude', 'create')
            ->allowEmptyString('longitude', false);

        $validator
            ->numeric('latitude')
            ->requirePresence('latitude', 'create')
            ->allowEmptyString('latitude', false);

        return $validator;
    }
}
