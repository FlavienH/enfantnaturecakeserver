<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

/**
 * Families Controller
 *
 *
 * @method \App\Model\Entity\Family[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FamiliesController extends AppController
{
    /**
     * this is the index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $families = $this->paginate($this->Families);

        $this->set(compact('families'));
    }


    /**
     * Function that return a family member with the id posted.
     * return is Json if an API asks
     */
    public function getFamilyMember(){
        
        if($this->isApi()){

            $data = $this->request->getData();
            $id = $data['id'];
            $familyMember = array();

            $queryFamilyMember = TableRegistry::get('Families')->find()->where(['id' => $id]);
            $queryFamilyMember = $queryFamilyMember->toArray()[0];
            

            $queryUser = TableRegistry::get('Users')->find()->where(['families_id' => $id]);
            $queryUser = $queryUser->toArray()[0];

            $familyMember['id'] = $queryUser['families_id']; 
            $familyMember['first_name'] = $queryUser['first_name'];
            $familyMember['last_name'] = $queryUser['last_name'];
            $familyMember['email'] = $queryUser['email'];
            $familyMember['role'] = $queryUser['role'];
            $familyMember['address'] = $queryFamilyMember['address'];
            $familyMember['phone_number'] = $queryFamilyMember['phone_number'];
            $familyMember['photo'] = $queryFamilyMember['photo'];
            $familyMember['language'] = $queryFamilyMember['language'];
            $familyMember['origin_country'] = $queryFamilyMember['origin_country'];
            $familyMember['in_canada_since'] = $queryFamilyMember['in_canada_since'];

            $this->set('familyMember', $familyMember);
                    $this->set('_serialize', 'familyMember');
                    return;
        }
    }

    /**
     * View method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $family = $this->Families->get($id, [
            'contain' => []
        ]);

        $this->set('family', $family);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $family = $this->Families->newEntity();
        if ($this->request->is('post')) {
            $family = $this->Families->patchEntity($family, $this->request->getData());
            if ($this->Families->save($family)) {
                $this->Flash->success(__('The family has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The family could not be saved. Please, try again.'));
        }
        $this->set(compact('family'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $family = $this->Families->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $family = $this->Families->patchEntity($family, $this->request->getData());
            if ($this->Families->save($family)) {
                $this->Flash->success(__('The family has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The family could not be saved. Please, try again.'));
        }
        $this->set(compact('family'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Family id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $family = $this->Families->get($id);
        if ($this->Families->delete($family)) {
            $this->Flash->success(__('The family has been deleted.'));
        } else {
            $this->Flash->error(__('The family could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Function that authorize the user to acces getFamilyMember.
     */
    public function isAuthorized($queryUser)
    {
        $action = $this->request->getParam('action');
        if($action == 'getFamilyMember')
        {
            return true;
        }
    }
}
