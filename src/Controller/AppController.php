<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\I18n;
use Cake\Collection\Collection;
use Cake\I18n\Time;
use Cake\I18n\FrozenTime;
use Cake\I18n\Date;
use Cake\I18n\FrozenDate;
use Cake\ORM\TableRegistry;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        
        //si API nécéssite une authentification.
        if($this->isApi()){
            $this->loadComponent('Auth', [
                'authenticate' => [
                    'Basic' => [
                        'fields' => ['username' => 'email', 'password' => 'password'],
                        'userModel' => 'Users'
                    ],
                ],
                'loginAction' => [
                    'controller' => 'Users',
                    'action' => 'login'
                ],
                //use isAuthorized in Controllers
                'authorize' => ['Controller'],
                
                'storage' => 'Memory',
                'unauthorizedRedirect' => false
            ]);

        }
    }


    public function isApi()
    {
        $url = $this->request->getAttribute("here");

        preg_match("(\w+)", $url, $matches);

        return array_key_exists(0,$matches) && $matches[0] == 'api';
    }

    public function beforeFilter(Event $event)
    {
    }

    protected function currentUser()
    {
        return TableRegistry::get('Users')->get($this->Auth->user('id'));
    }

    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');

        //By default, deny access.
        return false;
    }
}
