<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Tutors Controller
 *
 *
 * @method \App\Model\Entity\Tutor[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TutorsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $tutors = $this->paginate($this->Tutors);

        $this->set(compact('tutors'));
    }

    
    /**
     * Function that return a tutor with the id posted.
     * return is Json if an API asks
     */
    public function getTutor(){
        
        if($this->isApi()){

            $data = $this->request->getData();
            $id = $data['id'];
            $tutor = array();

            $queryTutor = TableRegistry::get('Tutors')->find()->where(['id' => $id]);
            $queryTutor = $queryTutor->toArray()[0];

            $queryUser = TableRegistry::get('Users')->find()->where(['tutors_id' => $id]);
            $queryUser = $queryUser->toArray()[0];

            $tutor['id'] = $queryUser['tutors_id']; 
            $tutor['first_name'] = $queryUser['first_name'];
            $tutor['last_name'] = $queryUser['last_name'];
            $tutor['email'] = $queryUser['email'];
            $tutor['role'] = $queryUser['role'];
            $tutor['photo'] = $queryTutor['photo'];
            $tutor['address'] = $queryTutor['address'];
            $tutor['phone_number'] = $queryTutor['phone_number'];

            $this->set('tutor', $tutor);
            $this->set('_serialize', 'tutor');
            return;
        }
    }

    /**
     * View method
     *
     * @param string|null $id Tutor id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tutor = $this->Tutors->get($id, [
            'contain' => []
        ]);

        $this->set('tutor', $tutor);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tutor = $this->Tutors->newEntity();
        if ($this->request->is('post')) {
            $tutor = $this->Tutors->patchEntity($tutor, $this->request->getData());
            if ($this->Tutors->save($tutor)) {
                $this->Flash->success(__('The tutor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tutor could not be saved. Please, try again.'));
        }
        $this->set(compact('tutor'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tutor id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tutor = $this->Tutors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tutor = $this->Tutors->patchEntity($tutor, $this->request->getData());
            if ($this->Tutors->save($tutor)) {
                $this->Flash->success(__('The tutor has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tutor could not be saved. Please, try again.'));
        }
        $this->set(compact('tutor'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Tutor id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tutor = $this->Tutors->get($id);
        if ($this->Tutors->delete($tutor)) {
            $this->Flash->success(__('The tutor has been deleted.'));
        } else {
            $this->Flash->error(__('The tutor could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Function that authorize the user to acces getTutor.
     */
    public function isAuthorized($queryUser)
    {
        $action = $this->request->getParam('action');
        if($action == 'getTutor')
        {
            return true;
        }
    }
}
