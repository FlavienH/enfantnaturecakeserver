<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Pins Controller
 *
 * @property \App\Model\Table\PinsTable $Pins
 *
 * @method \App\Model\Entity\Pin[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PinsController extends AppController
{
    /**
     * Index method
     *
     * retourne tous les pins présent dans la base de donnée.
     */
    public function index()
    {
        $this->set('pins', $this->Pins->find('all'));
        $this->set('_serialize', ['pins']);
    }

    /**
     * View method
     *
     * @param string|null $id Pin id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pin = $this->Pins->get($id, [
            'contain' => []
        ]);

        $this->set('pin', $pin);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $succes = false;
        $pin = $this->Pins->newEntity();
        if ($this->request->is('post')) {
            $pin = $this->Pins->patchEntity($pin, $this->request->getData());
            if ($this->Pins->save($pin)) {
                $success = true;
                
                $this->Flash->success(__('The pins has been saved.'));
            } else {
                $success = false;
                $this->Flash->error(__('The pins could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('pin', 'success'));
        $this->set('_serialize', ['success','pin']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pin id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {
        $id = $this->request->getData()['id'];
        $pin = $this->Pins->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pin = $this->Pins->patchEntity($pin, $this->request->getData());
            if ($this->Pins->save($pin)) {
                $success = true;
                
                $this->Flash->success(__('The pins has been saved.'));
            } else {
                $success = false;
                $this->Flash->error(__('The pins could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('pin', 'success'));
        $this->set('_serialize', ['success']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pin id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $succes = false;
        $this->request->allowMethod(['post', 'delete']);
        $id = $this->request->getData()['id'];
        $pin = $this->Pins->get($id);
        if ($this->Pins->delete($pin)) {
            $success = true;
                
            $this->Flash->success(__('The pins has been saved.'));
        } else {
            $success = false;
            $this->Flash->error(__('The pins could not be saved. Please, try again.'));
        }

        $this->set(compact('pin', 'success'));
        $this->set('_serialize', ['success']);
    }

        /**
     * Function that authorize the user to acces getKid.
     */
    public function isAuthorized($queryUser)
    {
        $action = $this->request->getParam('action');
        if($action == 'add' || 'delete' || 'edit' || 'index')
        {
            return true;
        }
    }
}
