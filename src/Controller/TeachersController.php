<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

/**
 * Teachers Controller
 *
 *
 * @method \App\Model\Entity\Teacher[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TeachersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $teachers = $this->paginate($this->Teachers);

        $this->set(compact('teachers'));
    }

    
    /**
     * Function that return a teacher with the id posted.
     * return is Json if an API asks
     */
    public function getTeacher(){
        
        if($this->isApi()){

            $data = $this->request->getData();
            $id = $data['id'];
            $teacher = array();

            $queryTeacher = TableRegistry::get('Teachers')->find()->where(['id' => $id]);
            $queryTeacher = $queryTeacher->toArray()[0];

            $queryUser = TableRegistry::get('Users')->find()->where(['teachers_id' => $id]);
            $queryUser = $queryUser->toArray()[0];

            $teacher['id'] = $queryUser['teachers_id']; 
            $teacher['first_name'] = $queryUser['first_name'];
            $teacher['last_name'] = $queryUser['last_name'];
            $teacher['email'] = $queryUser['email'];
            $teacher['role'] = $queryUser['role'];
            $teacher['photo'] = $queryTeacher['photo'];
            $teacher['address'] = $queryTeacher['address'];
            $teacher['phone_number'] = $queryTeacher['phone_number'];

            $this->set('teacher', $teacher);
            $this->set('_serialize', 'teacher');
        }
    }

    /**
     * View method
     *
     * @param string|null $id Teacher id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $teacher = $this->Teachers->get($id, [
            'contain' => []
        ]);

        $this->set('teacher', $teacher);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $teacher = $this->Teachers->newEntity();
        if ($this->request->is('post')) {
            $teacher = $this->Teachers->patchEntity($teacher, $this->request->getData());
            if ($this->Teachers->save($teacher)) {
                $this->Flash->success(__('The teacher has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The teacher could not be saved. Please, try again.'));
        }
        $this->set(compact('teacher', 'success'));
        $this->set('_serialize', ['success']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Teacher id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $teacher = $this->Teachers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $teacher = $this->Teachers->patchEntity($teacher, $this->request->getData());
            if ($this->Teachers->save($teacher)) {
                $this->Flash->success(__('The teacher has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The teacher could not be saved. Please, try again.'));
        }
        $this->set(compact('teacher'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Teacher id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $teacher = $this->Teachers->get($id);
        if ($this->Teachers->delete($teacher)) {
            $this->Flash->success(__('The teacher has been deleted.'));
        } else {
            $this->Flash->error(__('The teacher could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    /**
     * return to the API all the kids of the teacher's class
     */
    public function getGroup(){

        if($this->isApi()){
            $data = $this->request->getData();
            $id = $data['id'];

            $teacher = $this->Teachers->get($id, ['contain' => ['Groups']]);
            $groups =$teacher->groups;

            //dd($groupTab);
            $this->set('groups', $groups);
            $this->set('_serialize', ['groups']);
        }
    }

    /**
     * Function that authorize the user to acces getTeacher.
     */
    public function isAuthorized($queryUser)
    {
        $action = $this->request->getParam('action');
        if($action == 'getTeacher' || 'getGroup')
        {
            return true;
        }
    }
}
