<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

/**
 * Kids Controller
 *
 *
 * @method \App\Model\Entity\Kid[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class KidsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $kids = $this->paginate($this->Kids);

        $this->set(compact('kids'));
    }

    
    /**
     * Function that return a kid with the id posted.
     * return is Json if an API asks
     */
    public function getKid(){
        
        if($this->isApi()){

            $data = $this->request->getData();
            $id = $data['id'];
            $kid = array();

            $queryKid = TableRegistry::get('Kids')->find()->where(['id' => $id]);
            $queryKid = $queryKid->toArray()[0];

            $queryUser = TableRegistry::get('Users')->find()->where(['kids_id' => $id]);
            $queryUser = $queryUser->toArray()[0];

            $kid['id'] = $queryUser['kids_id']; 
            $kid['first_name'] = $queryUser['first_name'];
            $kid['last_name'] = $queryUser['last_name'];
            $kid['email'] = $queryUser['email'];
            $kid['role'] = $queryUser['role'];
            $kid['birth_date'] = $queryKid['birth_date'];
            $kid['address'] = $queryKid['address'];
            $kid['phone_number'] = $queryKid['phone_number'];
            $kid['photo'] = $queryKid['photo'];
            $kid['language'] = $queryKid['language'];
            $kid['origin_country'] = $queryKid['origin_country'];
            $kid['in_canada_since'] = $queryKid['in_canada_since'];
            $kid['health_issues'] = $queryKid['health_issues'];
            $kid['insurance_number'] = $queryKid['insurance_number'];

            $this->set('kid', $kid);
                    $this->set('_serialize', 'kid');
                    return;
        }
    }

    /**
     * View method
     *
     * @param string|null $id Kid id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $kid = $this->Kids->get($id, [
            'contain' => []
        ]);

        $this->set('kid', $kid);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $kid = $this->Kids->newEntity();
        if ($this->request->is('post')) {
            $kid = $this->Kids->patchEntity($kid, $this->request->getData());
            if ($this->Kids->save($kid)) {
                $this->Flash->success(__('The kid has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The kid could not be saved. Please, try again.'));
        }
        $this->set(compact('kid'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Kid id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $kid = $this->Kids->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $kid = $this->Kids->patchEntity($kid, $this->request->getData());
            if ($this->Kids->save($kid)) {
                $this->Flash->success(__('The kid has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The kid could not be saved. Please, try again.'));
        }
        $this->set(compact('kid'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Kid id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $kid = $this->Kids->get($id);
        if ($this->Kids->delete($kid)) {
            $this->Flash->success(__('The kid has been deleted.'));
        } else {
            $this->Flash->error(__('The kid could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Function that authorize the user to acces getKid.
     */
    public function isAuthorized($queryUser)
    {
        $action = $this->request->getParam('action');
        if($action == 'getKid')
        {
            return true;
        }
    }
}
