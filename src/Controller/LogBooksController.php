<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * LogBooks Controller
 *
 *
 * @method \App\Model\Entity\LogBook[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LogBooksController extends AppController
{
    /**
     * Index method
     *
     *  retourne tous les logbooks apparentant à un professeur ou un enfant, l'id ainsi que le role('teacher' ou autre) sont passé en post.
     */
    public function index()
    {
        if($this->isApi()){
            $data = $this->request->getData();
            if($data['role'] == 'teacher'){

                $logBook = $this->LogBooks->find('all')
                                            ->where(["teachers_id = :id"])
                                            ->bind(":id", $data['id'], 'integer')->toArray();
                
                $logBooks = array();
                foreach($logBook as $log){
                    //transforme le json en array
                    $content = json_decode($log['content'],true);

                    foreach($content as $key => $value){
                        if(strpos($key, 'image') !== false) {
                            $image = TableRegistry::get('Images')->find('all')->where(['Images.name' => $value])->toArray()[0];
                            $content[$key] = $image['image'];
                        }

                    }

                //on retransforme le l'array en string pour que l'application puisse le lire
                $log['content'] = json_encode($content);
                array_push($logBooks,$log);
                }

                $this->set('logBooks', $logBooks);
                $this->set('_serialize', ['logBooks']);

            }else{

                $logBook = $this->LogBooks->find('all')
                    ->where(["kids_id = :id"])
                    ->bind(":id", $data['id'], 'integer')->toArray();
                 $logBooks = array();

                foreach($logBook as $log){

                    $content = json_decode($log['content'],true);

                    foreach($content as $key => $value){
                        if(strpos($key, 'image') !== false) {
                            $image = TableRegistry::get('Images')->find('all')->where(['Images.name' => $value])->toArray()[0];
                            $content[$key] = $image['image'];
                        }

                    }

                    $log['content'] = json_encode($content);
                    array_push($logBooks,$log);
                }
                $this->set('logBooks', $logBooks);
                $this->set('_serialize', ['logBooks']);
            }
        }

    }

    /**
     * View method
     *
     * retourne un logbook à partir d'un id passé en post.
     */
    public function view()
    {
        $data = $this->request->getData();
        $id = $data['id'];
        $logBook = $this->LogBooks->get($id)->toArray();
        $content = json_decode($logBook['content'],true);

        foreach($content as $key => $value){
            if(strpos($key, 'image') !== false) {
                $image = TableRegistry::get('Images')->find('all')->where(['Images.name' => $value])->toArray()[0];
                $content[$key] = $image['image'];
            }

        }

        $logBook['content'] = json_encode($content);
        $this->set('logBook', $logBook);
        $this->set('_serialize', ['logBook']);
    }

    public function getAllLogBooks()
    {
        $logBooks = $this->LogBooks->find('all');
        $logBooks = $logBooks->toArray()[0];

        $this->set('logBooks', $logBooks);
        $this->set('_serialize', ['logBooks']);
    }
    /**
     * Add method
     *
     * Sauvegarde un logbook et les images qui lui sont associés dans la base de donnée. 
     * L'id de l'image est la date now, le nom du logbook et la place de l'image dans le logbook.
     */
    public function add()
    {
        $contentString = "" ;
        $log_book = $this->LogBooks->newEntity();
        $success = false;
        $count = 1;

        if ($this->getRequest()->is('post')) {
            $data = $this->request->getData();
            $content = $data['content'];
            foreach ($content as $key => $value) {
                if(strpos($key, 'image') !== false) {
                    $name = date('Ymd') . $data['name'] . $count;
                    $images = TableRegistry::get('Images');
                    $image = $images->newEntity([
                        'name' => $name,
                        'image' => $value,
                    ]);
                    $images->save($image);
                    $content[$key] = $name;
                    $count += 1;
                }
            }

            $contentString = json_encode($content);
            $data['content'] = $contentString;
            $log_book = $this->LogBooks->patchEntity($log_book, $data);
            if ($this->LogBooks->save($log_book)) {
                $success = true;
                
                $this->Flash->success(__('The log_book has been saved.'));
            } else {
                $success = false;
                $this->Flash->error(__('The log_book could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('log_books', 'success'));
        $this->set('_serialize', ['success']);
    }

    /**
     * Edit method
     *
     * Sauvegarde les changements effectués dans la Base de donnée, ajoute ou supprime les photos et dessins dans la table images
     */
    public function edit()
    {

        $contentString = "" ;
        $success = false;


        if ($this->request->is('post')) {

            $data = $this->request->getData();

            $log_book = $this->LogBooks->get($data['id']);
            $content = $data['content'];

            $data = $this->request->getData();
            $content = $data['content'];
            $count = 1;

            foreach (json_decode($log_book['content']) as $key => $log){
                if(strpos($key, 'image') !== false) {
                    $image = TableRegistry::get('Images')->find('all')->where(['Images.name' => $log])->toArray()[0];
                    $image = json_decode($image,true);
                    $images = TableRegistry::get('Images');
                    $entity = $images->patchEntity($images->newEntity(),$image);
                    $img = $images->get($image['id']);
                    $images->delete($img);
                    $count += 1;
                }
            }

            $count = 1;

            foreach ($content as $key => $value) {
                if(strpos($key, 'image') !== false) {
                    $name = date('Ymd') . $data['name'] . $count;
                    $images = TableRegistry::get('Images');
                    $image = $images->newEntity([
                        'name' => $name,
                        'image' => $value,
                    ]);
                    $images->save($image);
                    $content[$key] = $name;
                    $count += 1;
                }
            }

            $contentString = json_encode($content);
            $data['content'] = $contentString;
            $log_book = $this->LogBooks->patchEntity($log_book, $data);
            if ($this->LogBooks->save($log_book)) {
                $success = true;
                
            } else {
                $success = false;
            }
        }
        $this->set(compact('log_books', 'success'));
        $this->set('_serialize', ['success']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Log Book id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();


        $log_book = $this->LogBooks->get($data['id'])->toArray();
        
        $content = json_decode($log_book['content'],true);
        
        $count = 1;

        foreach ($content as $key => $log){
            if(strpos($key, 'image') !== false) {
                $image = TableRegistry::get('Images')->find('all')->where(['Images.name' => $log])->toArray()[0];
                $image = json_decode($image,true);
                $images = TableRegistry::get('Images');
                $entity = $images->patchEntity($images->newEntity(),$image);
                $img = $images->get($image['id']);
                $images->delete($img);
                $count += 1;
            }
        }


        $logBook = $this->LogBooks->get($data['id']);
        $success = false;
        if ($this->LogBooks->delete($logBook)) {
            $success = true;
            $this->Flash->success(__('The image book has been deleted.'));
        } else {
            $this->Flash->error(__('The image book could not be deleted. Please, try again.'));
        }

        $this->set(compact('success'));
        $this->set('_serialize', ['success']);
    }

    /**
     * Fonction qui authorise les personnes login à accéder aux pages suivantes.
     */
    public function isAuthorized($queryUser)
    {
        $action = $this->request->getParam('action');
        if($action == 'add' || $action == 'getAllLogBooks' || 'view' || 'edit')
        {
            return true;
        }
    }

    /**
     * fonction qui retrourne les logbooks en fonction du keyword passé en post ou ajax.
     */
    public function search()
    {   
        $this->getRequest()->allowMethod(['post', 'ajax', 'get']);
   
        //$keyword = "";


        /*if ($this->isApi()){
            $jsonData = $this->getRequest()->input('json_decode', true);
            //$keyword = $jsonData['keyword'] != null ? $jsonData['keyword'] : '';
        } else {
            $keyword = $this->getRequest()->getQuery('keyword') != null ? $this->getRequest()->getQuery('keyword') : '';
        }
        
        if($keyword == '')
        {
            $query = $this->Categories->find('all');
        }
        else
        {
            //query à faire en fonction de ce que l'on veut récupérer dans les logbooks.
            $query = $this->Categories->find('all')
            ->where(["match (name, description) against(:search in boolean mode)
                            or name like :like_search or description like :like_search"])
                            ->bind(":search", $keyword, 'string')
                            ->bind(":like_search", '%' . $keyword . '%', 'string');
                     
        }*/


        $query = $this->LogBooks->find('all');


        
        $this->set('logBooks', $this->paginate($query));
        $this->set('_serialize', ['logBooks']);
    }
}
