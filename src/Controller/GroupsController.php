<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Groups Controller
 *
 * qui est une classe scolaire exemple la classe 3B...
 * le mot classe n'a pas pu être pris parce qu'une class existe déjà dans cakephp.
 *
 * @method \App\Model\Entity\Group[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GroupsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $kids = $this->paginate($this->Groups);

        $this->set(compact('kids'));
    }

    /**
     * View method
     *
     * @param string|null $id Group id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $group = $this->Groups->get($id, [
            'contain' => []
        ]);

        $this->set('group', $group);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $group = $this->Groups->newEntity();
        if ($this->request->is('post')) {
            $group = $this->Groups->patchEntity($group, $this->request->getData());
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('The group has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The group could not be saved. Please, try again.'));
        }
        $this->set(compact('group'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Group id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $group = $this->Groups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $group = $this->Groups->patchEntity($group, $this->request->getData());
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('The group has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The group could not be saved. Please, try again.'));
        }
        $this->set(compact('group'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Group id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $group = $this->Groups->get($id);
        if ($this->Groups->delete($group)) {
            $this->Flash->success(__('The group has been deleted.'));
        } else {
            $this->Flash->error(__('The group could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * return all the kids from the group 
     */
    public function getKids(){

        if($this->isApi()){
            $data = $this->request->getData();
            $id = $data['id'];
            
            $kidsArray = array();

            $group = $this->Groups->get($id, ['contain' => ['Kids']]);
            $kidArray =$group->kids;


            foreach ($kidArray as $kid){

            $queryUser = TableRegistry::get('Users')->find()->where(['kids_id' => $kid->id]);
            $queryUser = $queryUser->toArray()[0];

            $kids['id'] = $queryUser['kids_id']; 
            $kids['first_name'] = $queryUser['first_name'];
            $kids['last_name'] = $queryUser['last_name'];
            $kids['email'] = $queryUser['email'];
            $kids['role'] = $queryUser['role'];
            $kids['birth_date'] = $kid['birth_date'];
            $kids['address'] = $kid['address'];
            $kids['phone_number'] = $kid['phone_number'];
            $kids['photo'] = $kid['photo'];
            $kids['language'] = $kid['language'];
            $kids['origin_country'] = $kid['origin_country'];
            $kids['in_canada_since'] = $kid['in_canada_since'];
            $kids['health_issues'] = $kid['health_issues'];
            $kids['insurance_number'] = $kid['insurance_number'];
        
                array_push($kidsArray,$kids);
            }
            $this->set('kidsArray', $kidsArray);
            $this->set('_serialize', ['kidsArray']);
        }
    }

    /**
     * Function that authorize the user to acces getTeacher.
     */
    public function isAuthorized($queryUser)
    {
        $action = $this->request->getParam('action');
        if($action == 'getKids')
        {
            return true;
        }
    }

}