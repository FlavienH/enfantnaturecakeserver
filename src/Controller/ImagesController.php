<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Images Controller
 *
 *
 * @method \App\Model\Entity\Image[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ImagesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $images = $this->paginate($this->Images);

        $this->set(compact('images'));
    }

    /**
     * View method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => []
        ]);

        $this->set('image', $image);
    }

    /**
     * Add method
     *
     * Sauvegarde les données passées en post dans la base de donnée.
     */
    public function add($id = null, $image = null)
    {
        $success = false;
        $image = $this->Images->newEntity();

        if ($this->request->is('post')) {
            $data= array();
            $data['id'] = $id;
            $data['image'] = $image;
            $image = $this->Images->patchEntity($image, $data);
            
            if ($this->Images->save($image)) {
                $success = true;
                
                $this->Flash->success(__('The log_book has been saved.'));
            } else {
                $success = false;
                $this->Flash->error(__('The log_book could not be saved. Please, try again.'));
            }
        $this->set(compact('image', 'success'));
        $this->set('_serialize', ['success']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $image = $this->Images->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $image = $this->Images->patchEntity($image, $this->request->getData());
            if ($this->Images->save($image)) {
                $this->Flash->success(__('The image has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The image could not be saved. Please, try again.'));
        }
        $this->set(compact('image'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Image id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $image = $this->Images->get($id);
        if ($this->Images->delete($image)) {
            $this->Flash->success(__('The image has been deleted.'));
        } else {
            $this->Flash->error(__('The image could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function isAuthorized($queryUser)
    {
        $action = $this->request->getParam('action');
        if($action == 'add')
        {
            return true;
        }
    }
}
