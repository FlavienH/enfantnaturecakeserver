<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Kid[]|\Cake\Collection\CollectionInterface $kids
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Kid'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tutors'), ['controller' => 'Tutors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tutor'), ['controller' => 'Tutors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Images'), ['controller' => 'Images', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Image'), ['controller' => 'Images', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Videos'), ['controller' => 'Videos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Video'), ['controller' => 'Videos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Posts'), ['controller' => 'Posts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post'), ['controller' => 'Posts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Families'), ['controller' => 'Families', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Family'), ['controller' => 'Families', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Trips'), ['controller' => 'Trips', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Trip'), ['controller' => 'Trips', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="kids index large-9 medium-8 columns content">
    <h3><?= __('Kids') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('password') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('birth_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('language') ?></th>
                <th scope="col"><?= $this->Paginator->sort('origin_country') ?></th>
                <th scope="col"><?= $this->Paginator->sort('in_canada_since') ?></th>
                <th scope="col"><?= $this->Paginator->sort('insurance_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tutors_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('images_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('videos_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('posts_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($kids as $kid): ?>
            <tr>
                <td><?= $this->Number->format($kid->id) ?></td>
                <td><?= h($kid->first_name) ?></td>
                <td><?= h($kid->last_name) ?></td>
                <td><?= h($kid->password) ?></td>
                <td><?= h($kid->email) ?></td>
                <td><?= h($kid->birth_date) ?></td>
                <td><?= h($kid->address) ?></td>
                <td><?= $this->Number->format($kid->phone_number) ?></td>
                <td><?= h($kid->language) ?></td>
                <td><?= h($kid->origin_country) ?></td>
                <td><?= h($kid->in_canada_since) ?></td>
                <td><?= $this->Number->format($kid->insurance_number) ?></td>
                <td><?= $kid->has('tutor') ? $this->Html->link($kid->tutor->id, ['controller' => 'Tutors', 'action' => 'view', $kid->tutor->id]) : '' ?></td>
                <td><?= $kid->has('image') ? $this->Html->link($kid->image->id, ['controller' => 'Images', 'action' => 'view', $kid->image->id]) : '' ?></td>
                <td><?= $kid->has('video') ? $this->Html->link($kid->video->id, ['controller' => 'Videos', 'action' => 'view', $kid->video->id]) : '' ?></td>
                <td><?= $kid->has('post') ? $this->Html->link($kid->post->id, ['controller' => 'Posts', 'action' => 'view', $kid->post->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $kid->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $kid->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $kid->id], ['confirm' => __('Are you sure you want to delete # {0}?', $kid->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
