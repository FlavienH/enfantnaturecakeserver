<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Kid $kid
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $kid->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $kid->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Kids'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tutors'), ['controller' => 'Tutors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tutor'), ['controller' => 'Tutors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Images'), ['controller' => 'Images', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Image'), ['controller' => 'Images', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Videos'), ['controller' => 'Videos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Video'), ['controller' => 'Videos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Posts'), ['controller' => 'Posts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post'), ['controller' => 'Posts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Families'), ['controller' => 'Families', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Family'), ['controller' => 'Families', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Trips'), ['controller' => 'Trips', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Trip'), ['controller' => 'Trips', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="kids form large-9 medium-8 columns content">
    <?= $this->Form->create($kid) ?>
    <fieldset>
        <legend><?= __('Edit Kid') ?></legend>
        <?php
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('password');
            echo $this->Form->control('email');
            echo $this->Form->control('birth_date');
            echo $this->Form->control('address');
            echo $this->Form->control('phone_number');
            echo $this->Form->control('photo');
            echo $this->Form->control('language');
            echo $this->Form->control('origin_country');
            echo $this->Form->control('in_canada_since', ['empty' => true]);
            echo $this->Form->control('health_issues');
            echo $this->Form->control('insurance_number');
            echo $this->Form->control('tutors_id', ['options' => $tutors]);
            echo $this->Form->control('images_id', ['options' => $images]);
            echo $this->Form->control('videos_id', ['options' => $videos]);
            echo $this->Form->control('posts_id', ['options' => $posts]);
            echo $this->Form->control('families._ids', ['options' => $families]);
            echo $this->Form->control('groups._ids', ['options' => $groups]);
            echo $this->Form->control('trips._ids', ['options' => $trips]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
