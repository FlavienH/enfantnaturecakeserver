<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Kid $kid
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Kid'), ['action' => 'edit', $kid->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Kid'), ['action' => 'delete', $kid->id], ['confirm' => __('Are you sure you want to delete # {0}?', $kid->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Kids'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Kid'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tutors'), ['controller' => 'Tutors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tutor'), ['controller' => 'Tutors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Images'), ['controller' => 'Images', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Image'), ['controller' => 'Images', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Videos'), ['controller' => 'Videos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Video'), ['controller' => 'Videos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Posts'), ['controller' => 'Posts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Post'), ['controller' => 'Posts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Families'), ['controller' => 'Families', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Family'), ['controller' => 'Families', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Groups'), ['controller' => 'Groups', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Group'), ['controller' => 'Groups', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Trips'), ['controller' => 'Trips', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Trip'), ['controller' => 'Trips', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="kids view large-9 medium-8 columns content">
    <h3><?= h($kid->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($kid->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($kid->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($kid->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($kid->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($kid->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Language') ?></th>
            <td><?= h($kid->language) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Origin Country') ?></th>
            <td><?= h($kid->origin_country) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tutor') ?></th>
            <td><?= $kid->has('tutor') ? $this->Html->link($kid->tutor->id, ['controller' => 'Tutors', 'action' => 'view', $kid->tutor->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Image') ?></th>
            <td><?= $kid->has('image') ? $this->Html->link($kid->image->id, ['controller' => 'Images', 'action' => 'view', $kid->image->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Video') ?></th>
            <td><?= $kid->has('video') ? $this->Html->link($kid->video->id, ['controller' => 'Videos', 'action' => 'view', $kid->video->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Post') ?></th>
            <td><?= $kid->has('post') ? $this->Html->link($kid->post->id, ['controller' => 'Posts', 'action' => 'view', $kid->post->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($kid->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone Number') ?></th>
            <td><?= $this->Number->format($kid->phone_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Insurance Number') ?></th>
            <td><?= $this->Number->format($kid->insurance_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Birth Date') ?></th>
            <td><?= h($kid->birth_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('In Canada Since') ?></th>
            <td><?= h($kid->in_canada_since) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Photo') ?></h4>
        <?= $this->Text->autoParagraph(h($kid->photo)); ?>
    </div>
    <div class="row">
        <h4><?= __('Health Issues') ?></h4>
        <?= $this->Text->autoParagraph(h($kid->health_issues)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Families') ?></h4>
        <?php if (!empty($kid->families)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('Prone Number') ?></th>
                <th scope="col"><?= __('Photo') ?></th>
                <th scope="col"><?= __('Lagages') ?></th>
                <th scope="col"><?= __('Origin Country') ?></th>
                <th scope="col"><?= __('In Canada Since') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($kid->families as $families): ?>
            <tr>
                <td><?= h($families->id) ?></td>
                <td><?= h($families->first_name) ?></td>
                <td><?= h($families->last_name) ?></td>
                <td><?= h($families->email) ?></td>
                <td><?= h($families->password) ?></td>
                <td><?= h($families->address) ?></td>
                <td><?= h($families->prone_number) ?></td>
                <td><?= h($families->photo) ?></td>
                <td><?= h($families->lagages) ?></td>
                <td><?= h($families->origin_country) ?></td>
                <td><?= h($families->in_canada_since) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Families', 'action' => 'view', $families->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Families', 'action' => 'edit', $families->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Families', 'action' => 'delete', $families->id], ['confirm' => __('Are you sure you want to delete # {0}?', $families->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Groups') ?></h4>
        <?php if (!empty($kid->groups)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Year') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($kid->groups as $groups): ?>
            <tr>
                <td><?= h($groups->id) ?></td>
                <td><?= h($groups->year) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Groups', 'action' => 'view', $groups->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Groups', 'action' => 'edit', $groups->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Groups', 'action' => 'delete', $groups->id], ['confirm' => __('Are you sure you want to delete # {0}?', $groups->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Trips') ?></h4>
        <?php if (!empty($kid->trips)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Start Date') ?></th>
                <th scope="col"><?= __('End Date') ?></th>
                <th scope="col"><?= __('Activities Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($kid->trips as $trips): ?>
            <tr>
                <td><?= h($trips->id) ?></td>
                <td><?= h($trips->start_date) ?></td>
                <td><?= h($trips->end_date) ?></td>
                <td><?= h($trips->activities_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Trips', 'action' => 'view', $trips->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Trips', 'action' => 'edit', $trips->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Trips', 'action' => 'delete', $trips->id], ['confirm' => __('Are you sure you want to delete # {0}?', $trips->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
