<div class="categories index large-12 medium-11 columns content">

    <div style="clear: both;"></div>

    <table id="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Content</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script>

$('document').ready(function(){
        $.ajax({
                method: 'post',
                url : "http://www.enfantnature.com:8080/api/logBooks/index.json",
                headers: {'Authorization' : "Basic ZmxhdmllbkBnbWFpbC5jb206cGFzc3dvcmQ=",  'X-CSRF-TOKEN': '<?=$this->getRequest()->getParam('_csrfToken');?>' },
                data: {id:"1",role:"enfant"},
                success: function( response ){
                    var table = $("#table tbody");
                    table.empty();
                    $.each(response.logBooks, function(idx, elem){
                        let idCell = "<td>" + elem.id + "</td>";
                        let nameCell = "<td>" + elem.name + "</td>";
                        let contentCell = "<td>" + elem.content + "</td>";

                        table.append("<tr>" + idCell + nameCell + contentCell + "</tr>");
                    });
                },
                error: function(jqXHR, textStatus, errorThrown){
                    alert("The association could not be deleted");
                    console.log(jqXHR.responseText);
                }
        });
    });
</script>

