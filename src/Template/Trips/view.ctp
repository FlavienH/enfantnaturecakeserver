<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Trip $trip
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Trip'), ['action' => 'edit', $trip->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Trip'), ['action' => 'delete', $trip->id], ['confirm' => __('Are you sure you want to delete # {0}?', $trip->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Trips'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Trip'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Activities'), ['controller' => 'Activities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Activity'), ['controller' => 'Activities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Kids'), ['controller' => 'Kids', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Kid'), ['controller' => 'Kids', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="trips view large-9 medium-8 columns content">
    <h3><?= h($trip->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Activity') ?></th>
            <td><?= $trip->has('activity') ? $this->Html->link($trip->activity->name, ['controller' => 'Activities', 'action' => 'view', $trip->activity->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($trip->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Date') ?></th>
            <td><?= h($trip->start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Date') ?></th>
            <td><?= h($trip->end_date) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Kids') ?></h4>
        <?php if (!empty($trip->kids)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Birth Date') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('Phone Number') ?></th>
                <th scope="col"><?= __('Photo') ?></th>
                <th scope="col"><?= __('Language') ?></th>
                <th scope="col"><?= __('Origin Country') ?></th>
                <th scope="col"><?= __('In Canada Since') ?></th>
                <th scope="col"><?= __('Health Issues') ?></th>
                <th scope="col"><?= __('Insurance Number') ?></th>
                <th scope="col"><?= __('Tutors Id') ?></th>
                <th scope="col"><?= __('Images Id') ?></th>
                <th scope="col"><?= __('Videos Id') ?></th>
                <th scope="col"><?= __('Posts Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($trip->kids as $kids): ?>
            <tr>
                <td><?= h($kids->id) ?></td>
                <td><?= h($kids->first_name) ?></td>
                <td><?= h($kids->last_name) ?></td>
                <td><?= h($kids->password) ?></td>
                <td><?= h($kids->email) ?></td>
                <td><?= h($kids->birth_date) ?></td>
                <td><?= h($kids->address) ?></td>
                <td><?= h($kids->phone_number) ?></td>
                <td><?= h($kids->photo) ?></td>
                <td><?= h($kids->language) ?></td>
                <td><?= h($kids->origin_country) ?></td>
                <td><?= h($kids->in_canada_since) ?></td>
                <td><?= h($kids->health_issues) ?></td>
                <td><?= h($kids->insurance_number) ?></td>
                <td><?= h($kids->tutors_id) ?></td>
                <td><?= h($kids->images_id) ?></td>
                <td><?= h($kids->videos_id) ?></td>
                <td><?= h($kids->posts_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Kids', 'action' => 'view', $kids->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Kids', 'action' => 'edit', $kids->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Kids', 'action' => 'delete', $kids->id], ['confirm' => __('Are you sure you want to delete # {0}?', $kids->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
