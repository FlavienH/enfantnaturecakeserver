<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tutor[]|\Cake\Collection\CollectionInterface $tutors
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tutor'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tutors index large-9 medium-8 columns content">
    <h3><?= __('Tutors') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tutors as $tutor): ?>
            <tr>
                <td><?= $this->Number->format($tutor->id) ?></td>
                <td><?= h($tutor->first_name) ?></td>
                <td><?= h($tutor->last_name) ?></td>
                <td><?= h($tutor->email) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $tutor->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tutor->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tutor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tutor->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
