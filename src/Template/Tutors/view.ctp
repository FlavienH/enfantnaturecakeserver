<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tutor $tutor
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tutor'), ['action' => 'edit', $tutor->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tutor'), ['action' => 'delete', $tutor->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tutor->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tutors'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tutor'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tutors view large-9 medium-8 columns content">
    <h3><?= h($tutor->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($tutor->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($tutor->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($tutor->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($tutor->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Photo') ?></h4>
        <?= $this->Text->autoParagraph(h($tutor->photo)); ?>
    </div>
</div>
