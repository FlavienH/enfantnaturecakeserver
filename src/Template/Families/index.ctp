<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family[]|\Cake\Collection\CollectionInterface $families
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Family'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Kids'), ['controller' => 'Kids', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Kid'), ['controller' => 'Kids', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="families index large-9 medium-8 columns content">
    <h3><?= __('Families') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('password') ?></th>
                <th scope="col"><?= $this->Paginator->sort('address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('prone_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lagages') ?></th>
                <th scope="col"><?= $this->Paginator->sort('origin_country') ?></th>
                <th scope="col"><?= $this->Paginator->sort('in_canada_since') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($families as $family): ?>
            <tr>
                <td><?= $this->Number->format($family->id) ?></td>
                <td><?= h($family->first_name) ?></td>
                <td><?= h($family->last_name) ?></td>
                <td><?= h($family->email) ?></td>
                <td><?= h($family->password) ?></td>
                <td><?= h($family->address) ?></td>
                <td><?= $this->Number->format($family->prone_number) ?></td>
                <td><?= h($family->lagages) ?></td>
                <td><?= h($family->origin_country) ?></td>
                <td><?= h($family->in_canada_since) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $family->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $family->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $family->id], ['confirm' => __('Are you sure you want to delete # {0}?', $family->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
