<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Family $family
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Family'), ['action' => 'edit', $family->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Family'), ['action' => 'delete', $family->id], ['confirm' => __('Are you sure you want to delete # {0}?', $family->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Families'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Family'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Kids'), ['controller' => 'Kids', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Kid'), ['controller' => 'Kids', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="families view large-9 medium-8 columns content">
    <h3><?= h($family->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($family->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($family->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($family->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($family->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($family->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lagages') ?></th>
            <td><?= h($family->lagages) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Origin Country') ?></th>
            <td><?= h($family->origin_country) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($family->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prone Number') ?></th>
            <td><?= $this->Number->format($family->prone_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('In Canada Since') ?></th>
            <td><?= h($family->in_canada_since) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Photo') ?></h4>
        <?= $this->Text->autoParagraph(h($family->photo)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Kids') ?></h4>
        <?php if (!empty($family->kids)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Birth Date') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('Phone Number') ?></th>
                <th scope="col"><?= __('Photo') ?></th>
                <th scope="col"><?= __('Language') ?></th>
                <th scope="col"><?= __('Origin Country') ?></th>
                <th scope="col"><?= __('In Canada Since') ?></th>
                <th scope="col"><?= __('Health Issues') ?></th>
                <th scope="col"><?= __('Insurance Number') ?></th>
                <th scope="col"><?= __('Tutors Id') ?></th>
                <th scope="col"><?= __('Images Id') ?></th>
                <th scope="col"><?= __('Videos Id') ?></th>
                <th scope="col"><?= __('Posts Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($family->kids as $kids): ?>
            <tr>
                <td><?= h($kids->id) ?></td>
                <td><?= h($kids->first_name) ?></td>
                <td><?= h($kids->last_name) ?></td>
                <td><?= h($kids->password) ?></td>
                <td><?= h($kids->email) ?></td>
                <td><?= h($kids->birth_date) ?></td>
                <td><?= h($kids->address) ?></td>
                <td><?= h($kids->phone_number) ?></td>
                <td><?= h($kids->photo) ?></td>
                <td><?= h($kids->language) ?></td>
                <td><?= h($kids->origin_country) ?></td>
                <td><?= h($kids->in_canada_since) ?></td>
                <td><?= h($kids->health_issues) ?></td>
                <td><?= h($kids->insurance_number) ?></td>
                <td><?= h($kids->tutors_id) ?></td>
                <td><?= h($kids->images_id) ?></td>
                <td><?= h($kids->videos_id) ?></td>
                <td><?= h($kids->posts_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Kids', 'action' => 'view', $kids->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Kids', 'action' => 'edit', $kids->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Kids', 'action' => 'delete', $kids->id], ['confirm' => __('Are you sure you want to delete # {0}?', $kids->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
