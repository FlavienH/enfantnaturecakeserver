
<script>
    function searchCategories( keyword ){
        //keyword est un mot passé dans la ajax request qui sert à dire ce qu'on cherche dans la BD.
        var data = keyword;
        $.ajax({
                method: 'get',
                url : "/logBooks/search.json",
                data: {keyword:data},
                success: function( response ){
                    // dans le cas du succès de la requete, on ajoute au tableau qui est la var table les elements du foreach avec le table.append
                    var table = $("#table tbody");
                    table.empty();
                    $.each(response.logBooks, function(idx, elem){
                        let idCell = "<td>" + elem.id + "</td>";
                        let nameCell = "<td>" + elem.name + "</td>";
                        let contentCell = "<td>" + elem.content + "</td>";

                        table.append("<tr>" + idCell + nameCell + contentCell + "</tr>");
                    });
                }
        });
    };
</script>